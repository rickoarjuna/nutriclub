import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

'Check if element ready'
WebUI.verifyElementVisible(findTestObject('CitCall/Icon/icon_img_citcall_dashboard'))

WebUI.delay(10)

'Click Menu with name "Miscall OTP"'
WebUI.click(findTestObject('CitCall/TextLink/textLink_menu_Miscall OTP'))

WebUI.delay(10)

'Click element sub menu from menu Miscall OTP with name "Logs"'
WebUI.click(findTestObject('CitCall/TextLink/textLink_submenu_Logs'))

WebUI.delay(10)

'Verify the element'
WebUI.verifyElementVisible(findTestObject('CitCall/Text/text_MISCALL OTP - Logs'))

'set text for input type element'
WebUI.sendKeys(findTestObject('CitCall/TextInput/input_Msisdn'), '6281280158820')

WebUI.click(findTestObject('CitCall/Button/button_Apply Filters'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(10)

WebDriver driver = DriverFactory.getWebDriver()

'Expected value from Table'
String ExpectedValue = '+6281280158820'

'To locate table'
WebElement Table = driver.findElement(By.xpath('//table/tbody'))

'To locate rows of table it will Capture all the rows available in the table'
List<WebElement> rows_table = Table.findElements(By.tagName('tr'))

'To calculate no of rows In table'
int rows_count = rows_table.size()

String token_OTP = ''

'Loop will execute for all the rows of the table'
Loop: for (int row = 0; row < rows_count; row++) {
    'To locate columns(cells) of that specific row'
    List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName('td'))

    'To calculate no of columns(cells) In that specific row'
    int columns_count = Columns_row.size()

    println((('Number of cells In Row ' + row) + ' are ') + columns_count)

    'Loop will execute till the last cell of that specific row'
    for (int column = 0; column < columns_count; column++) {
        'It will retrieve text from each cell'
        String celltext = Columns_row.get(column).getText()

        println((((('Cell Value Of row number ' + row) + ' and column number ') + column) + ' Is ') + celltext)

        'Checking if Cell text is matching with the expected value'
        if (celltext == ExpectedValue) {
            'Getting the Country Name if cell text i.e Company name matches with Expected value'
            println('Text present in row number 3 is: ' + Columns_row.get(2).getText())

            token_OTP = Columns_row.get(2).getText().substring(7)

            'After getting the Expected value from Table we will Terminate the loop'
            Loop: break
        }
    }
}

//021 1234 0808
System.out.println(token_OTP)

def token1 = token_OTP.substring(0, 1)

def token2 = token_OTP.substring(1, 2)

def token3 = token_OTP.substring(2, 3)

def token4 = token_OTP.substring(3, 4)

System.out.println(token1)

System.out.println(token2)

System.out.println(token3)

System.out.println(token4)

