import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.verifyElementVisible(findTestObject('NutriClub/Text/text_Isi dan lengkapi data diri'))

WebUI.sendKeys(findTestObject('NutriClub/TextInput/input_ddmmyyyy'), GlobalVariable.ChildBirthDate)

WebUI.sendKeys(findTestObject('NutriClub/TextInput/input_Nama Anak'), GlobalVariable.ChildName)

WebUI.sendKeys(findTestObject('NutriClub/TextInput/input_Nama Orang Tua'), GlobalVariable.FullName)

WebUI.sendKeys(findTestObject('NutriClub/TextInput/input_Alamat Email'), GlobalVariable.Email)

WebUI.sendKeys(findTestObject('NutriClub/TextInput/input_Nomor Handphone'), GlobalVariable.Phone)

WebUI.sendKeys(findTestObject('NutriClub/TextInput/input_Kata Sandi_password'), GlobalVariable.Password)

WebUI.sendKeys(findTestObject('NutriClub/TextInput/input_Konfirmasi Password'), GlobalVariable.Password)

WebUI.check(findTestObject('NutriClub/RadioButton/radioButton_tidak'))

WebUI.verifyElementChecked(findTestObject('NutriClub/RadioButton/radioButton_tidak'), 30)

WebUI.check(findTestObject('NutriClub/CheckBox/checkBox_IsAgree'))

WebUI.verifyElementChecked(findTestObject('NutriClub/CheckBox/checkBox_IsAgree'), 30)

WebUI.verifyElementClickable(findTestObject('NutriClub/Button/button_Selanjutnya Register2'))

WebUI.click(findTestObject('NutriClub/Button/button_Selanjutnya Register2'))

