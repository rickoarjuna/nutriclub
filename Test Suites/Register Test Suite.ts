<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Register Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>c24d55df-454e-409e-8dfe-90cbaa8bba0c</testSuiteGuid>
   <testCaseLink>
      <guid>d4d244dd-b5a4-42cc-8b7a-0e9df199e88e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Open browser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9ccf70de-e953-4dd5-a5ca-96b542360d0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Page Login</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a3b1f76f-7ab8-4c3e-a2b4-be8e8e66b771</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Choose Mom or Dad</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>182426c1-b7e9-4492-8bcd-5ffb4553855e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Choose Condition</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1a64d798-156c-4a9b-80b7-6651b663bcea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Register</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7130da03-7173-405e-9e3a-1caafc23e548</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Miscall User</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
